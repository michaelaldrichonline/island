extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	transform.basis = transform.basis.rotated(axis, rand_range(1, 22))
	var area = get_node("Area")
	area.connect("input_event", self, "_on_area_input_event")


# Called every frame. 'delta' is the elapsed time since the previous frame.
var per_frame = rand_range(0.8, 2)
var axis = Vector3(0, 1, 0)
func _process(delta):
	var this_frame = per_frame * delta
	transform.basis = transform.basis.rotated(axis, this_frame)

func _on_area_input_event(_camera, event, _click_position, _click_normal, _shape_idx):
	if not event is InputEventMouseButton:
		return
	if not event.pressed:
		return
	if not event.button_index == BUTTON_LEFT:
		return
	place_cam()

func _after_timeout():
	var sky_cam = get_node("%SkyCam")
	sky_cam.current = true

func place_cam():
	var cam = get_node("%GroundCam")
	var new_transform = self.transform.translated(Vector3(0, 0, 5))
	new_transform = new_transform.looking_at(self.translation, Vector3(0, 1, 0))
	cam.transform = new_transform
	cam.current = true
	var reset_timer = Timer.new()
	reset_timer.wait_time = 2.0
	reset_timer.one_shot = true
	reset_timer.connect("timeout", self, "_after_timeout")
	add_child(reset_timer)
	reset_timer.start()
